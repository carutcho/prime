package br.com.prime.commons.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.http.HttpStatus;

public class RespostaPadraoJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private HttpStatus status;
	private Collection<Object> retorno;
	private Collection<Mensagem> mensagens;
	
	public RespostaPadraoJson(HttpStatus status) {
		this.status = status;
	}
	
	public RespostaPadraoJson(HttpStatus status, Mensagem mensagem) {
		this.status = status;
		this.mensagens = gerarMensagem(mensagem);
	}
	
	public RespostaPadraoJson(HttpStatus status, Mensagem mensagem, Collection<Object> retorno) {
		this.status = status;
		this.mensagens = gerarMensagem(mensagem);
		this.retorno = retorno;
	}
	
	private List<Mensagem> gerarMensagem(Mensagem mensagem){
		List<Mensagem> mensagens = new ArrayList<Mensagem>();
		mensagens.add(mensagem);
		return mensagens;
	}
	
	public RespostaPadraoJson(HttpStatus status, Collection<Object> retorno) {
		this.status = status;
		this.retorno = retorno;
	}

	public Collection<Object> getRetorno() {
		return retorno;
	}
	
	public void setRetorno(Collection<Object> retorno) {
		this.retorno = retorno;
	}
	
	public HttpStatus getStatus() {
		return status;
	}
	
	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public Collection<Mensagem> getMensagens() {
		return mensagens;
	}

	public void setMensagem(Collection<Mensagem> mensagens) {
		this.mensagens = mensagens;
	}
}
