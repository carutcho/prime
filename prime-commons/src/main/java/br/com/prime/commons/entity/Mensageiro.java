package br.com.prime.commons.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.prime.commons.data.persistence.Persistent;
import br.com.prime.commons.enums.LocaleEnum;
import br.com.prime.commons.enums.TipoMensagemEnum;
import br.com.prime.commons.mensagens.MensagensMensageiro;

@Entity
@Table(name = "mensageiro", uniqueConstraints = { @UniqueConstraint( columnNames = {"key","codigo","locale"} ) })
public class Mensageiro implements Persistent{
    
	private static final long serialVersionUID = 1L;

	@Id 
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
    private Long id;
	
	@NotEmpty(message = MensagensMensageiro.ERRO_KEY_VAZIO)
	@Column(name="key")
	private String key;
	
	@NotEmpty(message = MensagensMensageiro.ERRO_MENSAGEM_VAZIO)
	@Column(name="mensagem")
    private String mensagem;

	@NotEmpty(message = MensagensMensageiro.ERRO_LOCALE_VAZIO)
	@Column(name = "locale")
    protected LocaleEnum locale = LocaleEnum.BR;
    
	@NotEmpty(message = MensagensMensageiro.ERRO_TIPO_VAZIO)
	@Column(name = "tipo")
    protected TipoMensagemEnum tipo = TipoMensagemEnum.INFO;

	@NotEmpty(message = MensagensMensageiro.ERRO_CODIGO_VAZIO)
	@Column(name = "codigo")
	private Integer codigo;
	
	public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public LocaleEnum getLocale() {
		return locale;
	}

	public void setLocale(LocaleEnum locale) {
		this.locale = locale;
	}

	public TipoMensagemEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoMensagemEnum tipo) {
		this.tipo = tipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mensageiro other = (Mensageiro) obj;
		
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getLabel() {
		return "";
	}

	public String getName() {
		return null;
	}

	public Integer getCodigo() {
		return this.codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	
}