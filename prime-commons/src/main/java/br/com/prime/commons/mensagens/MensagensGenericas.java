package br.com.prime.commons.mensagens;

public class MensagensGenericas {

	public static final String ERRO_FALHA_GENERICA 			= "msg.erro.generica.falha.generica";
	public static final String ERRO_NAO_ENCONTRADO   		= "msg.erro.generica.nao.encontrado.atualizar";
	public static final String ERRO_REGISTRO_EXISTENTE		= "msg.erro.generica.regstro.existente";
	public static final String ERRO_FALHA_INSERIR 			= "msg.erro.generica.falha.inserir";
	public static final String ERRO_FALHA_ATUALIZAR 		= "msg.erro.generica.falha.atualizar";	
	public static final String ERRO_FALHA_REMOVER 			= "msg.erro.generica.falha.remover";
	public static final String ERRO_PARAMENTO_MENOR_ZERO 	= "msg.erro.generica.paramento.menor.zero";
	public static final String SUCESSO_INSERIR 				= "msg.sucesso.generica.inserir";
	public static final String SUCESSO_ATUALIZAR 			= "msg.sucesso.generica.atualizar";
	public static final String SUCESSO_REMOVER 				= "msg.sucesso.generica.remover";

}
