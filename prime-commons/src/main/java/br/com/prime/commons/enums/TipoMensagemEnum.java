package br.com.prime.commons.enums;

public enum TipoMensagemEnum {
	ERRO("Error"),
	INFO("Info"),
	WARNING("Warning"),
	SUCESSO("Sucess");
	
	private String label;
	
	private TipoMensagemEnum(String label){
		this.label = label;
	}
	
	public String getLabel(){
		return label;
	}
}