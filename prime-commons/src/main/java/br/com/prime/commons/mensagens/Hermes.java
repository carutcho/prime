package br.com.prime.commons.mensagens;

import java.util.Collection;
import java.util.HashSet;

import br.com.prime.commons.entity.Mensageiro;

/***
 * 
 * @author Carutcho
 * Singleton mensageiro, responsável por armazenar todas as mensagens do sistema em memória, para evitar necessidade de 
 * fazer uma consulta no banco todas as vezes que precisamos buscar uma mensagem
 * Existe uma classe responsável por buscar e atualizar esse singleton
 */
public class Hermes {
	
	private static Hermes instance = null;
	private static Object mutex = new Object();
	private static HashSet<Mensageiro> mensagens = new HashSet<Mensageiro>();
	
	private Hermes() {
	}
	
	public static Hermes getInstance() {
		Hermes result = instance;
		if (result == null) {
			synchronized (mutex) {
				result = instance;
				if (result == null)
					instance = result = new Hermes();
			}
		}
		return result;
	}

	public HashSet<Mensageiro> getMensagens() {
		return mensagens;
	}

	public static void setMensagens(HashSet<Mensageiro> msgAtualizadas) {
		mensagens = msgAtualizadas;
	}
	
}
