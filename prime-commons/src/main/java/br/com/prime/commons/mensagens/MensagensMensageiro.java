package br.com.prime.commons.mensagens;

public class MensagensMensageiro {

	public static final String ERRO_KEY_VAZIO 			= "msg.erro.mensageiro.key.vazio";
	public static final String ERRO_MENSAGEM_VAZIO 		= "msg.erro.mensageiro.mensagem.vazio";
	public static final String ERRO_LOCALE_VAZIO 		= "msg.erro.mensageiro.locale.vazio";
	public static final String ERRO_TIPO_VAZIO 			= "msg.erro.mensageiro.tipo.vazio";
	public static final String ERRO_CODIGO_VAZIO 		= "msg.erro.mensageiro.codigo.vazio";
	public static final String ERRO_KEY_NAO_ENCONTRADA	= "msg.erro.mensageiro.key.nao.encontrada";
	public static final String ERRO_KEY_NAO_CADASTRADA	= "msg.erro.mensageiro.key.nao.cadastrada";
	public static final String ERRO_SEM_MSG_BD			= "Falha ao resgatar a mensagem";
	
}
