package br.com.prime.commons.mensagens;

public class MensagensProduto {

	public static final String ERRO_NOME_VAZIO 			= "msg.erro.produto.nome.vazio";
	public static final String ERRO_DESCRICAO_VAZIO 	= "msg.erro.produto.descricao.vazio";
	public static final String ERRO_PRODUTO_EXISTENTE 	= "msg.erro.produto.existente";
	
	
}
