package br.com.prime.commons.enums;

public enum LocaleEnum {
	BR("BR"),
	US("US"),
	SP("SP");
	
	private String label;
	
	private LocaleEnum(String label){
		this.label = label;
	}
	
	public String getLabel(){
		return label;
	}
}