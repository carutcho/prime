package br.com.prime.services.regras.mensageiro;


import static br.com.prime.commons.utils.Utils.isEmpty;

import java.util.ArrayList;
import java.util.Collection;

import br.com.prime.commons.enums.LocaleEnum;
import br.com.prime.commons.mensagens.MensagensMensageiro;
import br.com.prime.commons.regras.RegraDeNegocioImpl;
import br.com.prime.commons.vo.Mensagem;
import br.com.prime.data.interfaces.MensageiroDAO;

public class BuscarMensagens extends RegraDeNegocioImpl<Mensagem> {

	Collection<String> keys;
	MensageiroDAO dao;
	LocaleEnum locale = LocaleEnum.BR;
	
	public BuscarMensagens(Collection<String> keys, MensageiroDAO dao){
		this.keys = keys;
		this.dao = dao;
	}
	
	public BuscarMensagens(Collection<String> keys, MensageiroDAO dao, LocaleEnum locale){
		this.keys = keys;
		this.dao = dao;
		this.locale = locale;
	}
	
	public ArrayList<Mensagem> executarRetornoLista() {
		ArrayList<Mensagem> mensagens = new ArrayList<Mensagem>();		
		if (isEmpty(keys)){
			logger.error(MensagensMensageiro.ERRO_CODIGO_VAZIO);
		}else{	
			for (String key : keys) {
				Mensagem mensagem = new BuscarMensagem(key, dao, locale).executarRetorno(); 
				mensagens.add(mensagem);
			}
		}
		return mensagens;	
	}
}
