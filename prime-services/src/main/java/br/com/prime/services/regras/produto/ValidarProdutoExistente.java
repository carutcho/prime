package br.com.prime.services.regras.produto;


import static br.com.prime.commons.utils.Utils.isNotEmpty;

import br.com.prime.commons.entity.Produto;
import br.com.prime.commons.exceptions.ServiceBusinessException;
import br.com.prime.commons.mensagens.MensagensGenericas;
import br.com.prime.commons.mensagens.MensagensProduto;
import br.com.prime.commons.regras.RegraDeNegocioImpl;
import br.com.prime.data.exception.PersistenceValidateException;
import br.com.prime.data.interfaces.ProdutoDAO;

public class ValidarProdutoExistente extends RegraDeNegocioImpl<Produto> {

	Produto produto;
	ProdutoDAO dao;
	
	public ValidarProdutoExistente(Produto produto, ProdutoDAO dao) throws ServiceBusinessException {
		this.produto = produto;
		this.dao = dao;
		this.executar();
	}
	
	@Override
	public void executar() throws ServiceBusinessException {
		try {
			Produto produtoBanco = dao.buscarPorId(produto.getId());
			if (isNotEmpty(produtoBanco)){
				throw new ServiceBusinessException(MensagensProduto.ERRO_PRODUTO_EXISTENTE);
			}
		} catch (PersistenceValidateException e) {		
			logger.error("msg.erro.produto.encontrado" + produto.toString());
			throw new ServiceBusinessException(getProperties().getProperty(MensagensGenericas.ERRO_FALHA_GENERICA));
		}
	}
}
