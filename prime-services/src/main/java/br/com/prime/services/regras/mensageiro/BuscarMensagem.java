package br.com.prime.services.regras.mensageiro;


import static br.com.prime.commons.utils.Utils.isEmpty;
import static br.com.prime.commons.utils.Utils.isNotEmpty;

import br.com.prime.commons.entity.Mensageiro;
import br.com.prime.commons.enums.LocaleEnum;
import br.com.prime.commons.exceptions.ServiceBusinessException;
import br.com.prime.commons.mensagens.MensagensMensageiro;
import br.com.prime.commons.regras.RegraDeNegocioImpl;
import br.com.prime.commons.vo.Mensagem;
import br.com.prime.data.exception.PersistenceValidateException;
import br.com.prime.data.interfaces.MensageiroDAO;

public class BuscarMensagem extends RegraDeNegocioImpl<Mensageiro> {

	
	private BuscarMensagemHash hash;
	private String key;
	private MensageiroDAO dao;
	private LocaleEnum locale = LocaleEnum.BR;
	
	public BuscarMensagem(String key, MensageiroDAO dao) {
		this.key = key;
		this.dao = dao;
		this.hash = new BuscarMensagemHash(key, dao);
	}
	
	public BuscarMensagem(String key, MensageiroDAO dao, LocaleEnum locale) {
		this.key = key;
		this.dao = dao;
		this.locale = locale;
		this.hash = new BuscarMensagemHash(key, dao, this.locale);
	}
	
	public Mensagem executarRetorno() {
		
		Mensagem retorno = null;
		try {
			if (isEmpty(key)){
				logger.error(MensagensMensageiro.ERRO_KEY_VAZIO);
			}
			
			retorno = hash.executarRetorno();
			
			if(isEmpty(retorno)) {
				retorno = gerarMensagemKeyNaoCadastrada(key);
			}
			
		} catch (Exception e) {		
			logger.error(MensagensMensageiro.ERRO_KEY_NAO_ENCONTRADA);
		}
		return retorno;
	}

	private Mensagem gerarMensagemKeyNaoCadastrada(String keyOriginal) throws PersistenceValidateException, ServiceBusinessException {
		
		Mensagem retorno;
		String keyMsgErro = MensagensMensageiro.ERRO_KEY_NAO_CADASTRADA;
		this.hash = new BuscarMensagemHash(keyMsgErro, dao, locale);
		
		Mensagem mensagem = hash.executarRetorno();
		
		if (isNotEmpty(mensagem)){
			logger.error(String.format(mensagem.getMensagem(), keyOriginal));
			retorno = new Mensagem(mensagem.getCodigo(), String.format(mensagem.getMensagem(), keyOriginal));
		}else{
			logger.error(String.format(mensagem.getMensagem(), keyOriginal)); //sem cadastro da key original
			logger.error(String.format(mensagem.getMensagem(), keyMsgErro)); //sem cadastro da key de erro
			retorno = new Mensagem(99, MensagensMensageiro.ERRO_SEM_MSG_BD);
		}
		
		return retorno;
	}
}
