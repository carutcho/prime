package br.com.prime.services.regras.mensageiro;


import static br.com.prime.commons.utils.Utils.isEmpty;
import static br.com.prime.commons.utils.Utils.isNotEmpty;

import java.util.Collection;
import java.util.HashSet;

import br.com.prime.commons.entity.Mensageiro;
import br.com.prime.commons.entity.Produto;
import br.com.prime.commons.enums.LocaleEnum;
import br.com.prime.commons.exceptions.ServiceBusinessException;
import br.com.prime.commons.mensagens.Hermes;
import br.com.prime.commons.mensagens.MensagensMensageiro;
import br.com.prime.commons.regras.RegraDeNegocioImpl;
import br.com.prime.commons.vo.Mensagem;
import br.com.prime.data.exception.PersistenceValidateException;
import br.com.prime.data.interfaces.MensageiroDAO;

public class BuscarMensagemHash extends RegraDeNegocioImpl<Produto> {

	String key;
	MensageiroDAO dao;
	LocaleEnum locale = LocaleEnum.BR;
	Hermes hermes = Hermes.getInstance();
	
	public BuscarMensagemHash(String key, MensageiroDAO dao) {
		this.key = key;
		this.dao = dao;
	}
	
	public BuscarMensagemHash(String key, MensageiroDAO dao, LocaleEnum locale) {
		this.key = key;
		this.dao = dao;
		this.locale = locale;
	}
	
	public Mensagem executarRetorno() throws ServiceBusinessException {
		
		Mensagem retorno = null;
		
			if (isEmpty(key)){
				logger.error(MensagensMensageiro.ERRO_KEY_VAZIO);
			}
			
			Mensageiro msg = buscarNoHash(key);
			if(isNotEmpty(msg)) {
				retorno = gerarMensagem(msg);	
			}else{
				atualizarHashMensagens();
				msg = buscarNoHash(key);
				if (isNotEmpty(msg)){
					retorno = gerarMensagem(msg);
				}
			}
		
		return retorno;
	}

	private Mensagem gerarMensagem(Mensageiro msg) {
		return new Mensagem(msg.getCodigo(), msg.getMensagem());
	}
	
	private void atualizarHashMensagens() throws ServiceBusinessException{
		try {
			HashSet<Mensageiro> mensagens = new HashSet<Mensageiro>((Collection<? extends Mensageiro>) dao.listarTodos());
			hermes.setMensagens(mensagens);
		} catch (PersistenceValidateException e) {
			logger.error(MensagensMensageiro.ERRO_KEY_NAO_ENCONTRADA);
			throw new ServiceBusinessException(e.getMessage());
		}
	}
	
	private Mensageiro buscarNoHash(String key){		
		
		Mensageiro retorno = null;
		Collection<Mensageiro> mensagens = hermes.getMensagens();
		
		if (isNotEmpty(mensagens)){
			for (Mensageiro mensageiro : mensagens) {
				if (mensageiro.getKey().equals(key) && mensageiro.getLocale().equals(locale)){
					retorno = mensageiro;
					break;
				}
			}
		}
		return retorno;		
	}

}
