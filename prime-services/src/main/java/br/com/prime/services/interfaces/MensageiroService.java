package br.com.prime.services.interfaces;

import java.util.Collection;

import br.com.prime.commons.entity.Mensageiro;
import br.com.prime.commons.enums.LocaleEnum;
import br.com.prime.commons.vo.Mensagem;
import br.com.prime.data.exception.PersistenceValidateException;
import br.com.prime.services.base.CrudService;

public interface MensageiroService extends CrudService<Mensageiro>{

	public Mensageiro buscarPorKey(String key) throws PersistenceValidateException;
	
	public Collection<Mensagem> buscarMensagens(Collection<String> keys);
	
	public Collection<Mensagem> buscarMensagens(Collection<String> keys, LocaleEnum locale);
	
	public Mensagem buscarMensagem(String keys);
	
	public Mensagem buscarMensagem(String keys, LocaleEnum locale);
}
