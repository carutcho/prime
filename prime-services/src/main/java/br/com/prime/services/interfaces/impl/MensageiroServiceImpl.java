package br.com.prime.services.interfaces.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.prime.commons.entity.Mensageiro;
import br.com.prime.commons.enums.LocaleEnum;
import br.com.prime.commons.exceptions.ServiceBusinessException;
import br.com.prime.commons.vo.Mensagem;
import br.com.prime.data.exception.PersistenceValidateException;
import br.com.prime.data.interfaces.MensageiroDAO;
import br.com.prime.services.base.CrudServiceImpl;
import br.com.prime.services.interfaces.MensageiroService;
import br.com.prime.services.regras.mensageiro.BuscarMensagem;
import br.com.prime.services.regras.mensageiro.BuscarMensagens;


@Service
public class MensageiroServiceImpl extends CrudServiceImpl<Mensageiro, MensageiroDAO> implements MensageiroService{

	private static final long serialVersionUID = 1L;
	
	@Autowired
	public MensageiroServiceImpl(MensageiroDAO dao) {
		super(dao);
	}

	public Mensageiro buscarPorKey(String key) throws PersistenceValidateException{
		return dao.buscarPorKey(key);
	}

	public Collection<Mensagem> buscarMensagens(Collection<String> keys) {
		return this.buscarMensagens(keys, LocaleEnum.BR);
	}

	public Mensagem buscarMensagem(String key){
		return buscarMensagem(key, LocaleEnum.BR);
	}

	public Collection<Mensagem> buscarMensagens(Collection<String> keys, LocaleEnum locale) {
		return new BuscarMensagens(keys, dao, locale).executarRetornoLista();
	}

	public Mensagem buscarMensagem(String key, LocaleEnum locale) {
		return new BuscarMensagem(key, dao, locale).executarRetorno();
	}
}
