package br.com.prime.data.interfaces.impl;

import org.springframework.stereotype.Repository;

import br.com.prime.commons.entity.Mensageiro;
import br.com.prime.data.exception.PersistenceValidateException;
import br.com.prime.data.interfaces.MensageiroDAO;
import br.com.prime.data.persistence.hibernate.HibernateTemplateCrudDao;

@Repository
public class MensageiroDAOImp extends HibernateTemplateCrudDao<Mensageiro> implements MensageiroDAO{

	private static final long serialVersionUID = 1L;

	public Mensageiro buscarPorKey(String key) throws PersistenceValidateException {
		Mensageiro retorno = (Mensageiro) getSession()
				.createQuery("SELECT m FROM Mensageiro m WHERE m.key = :key")
				.setParameter("key",key)
				.setMaxResults(1)
				.uniqueResult();

		return  retorno;
		
	}
}
