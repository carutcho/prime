package br.com.prime.data.interfaces;

import br.com.prime.commons.entity.Mensageiro;
import br.com.prime.data.exception.PersistenceValidateException;
import br.com.prime.data.persistence.CrudDao;

public interface MensageiroDAO extends CrudDao<Mensageiro> {

	public Mensageiro buscarPorKey(String key) throws PersistenceValidateException;

}
