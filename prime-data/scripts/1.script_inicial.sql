-- INSERT EM TABELA DE PERFIS
INSERT INTO perfil (id, nome, sigla, admin) VALUES 
(1, 'Administrador', 'ROLE_ADMIN', true),
(2, 'Cliente', 'ROLE_CLIENTE', false);

-- TABELA DE USUARIO
INSERT INTO usuario (id, ativo, descricao, login, senha, locale) VALUES 
(1, true, 'usuario main', 'admin', '$2a$10$XfzM8Buv.ca582ID6m1wDOx/OZapqXynJ0Vf6If6T9W3fiuBi0V/O',0),
(2, true, 'usuario main', 'admin-us', '$2a$10$XfzM8Buv.ca582ID6m1wDOx/OZapqXynJ0Vf6If6T9W3fiuBi0V/O',1);
